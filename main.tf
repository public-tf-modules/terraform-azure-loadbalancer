# Azure load balancer module

resource "azurerm_lb" "azlb" {
  name                = var.name
  resource_group_name = var.resource_group_name
  location            = var.location
  tags                = var.tags
  sku                 = var.sku

  dynamic "frontend_ip_configuration"  {
    for_each = var.frontend
    content {
      name      = frontend_ip_configuration.key
      subnet_id = frontend_ip_configuration.value.subnet_id 
      private_ip_address = lookup(frontend_ip_configuration.value, "private_ip_address", null)
      private_ip_address_allocation = lookup(frontend_ip_configuration.value, "private_ip_address_allocation", null)
      public_ip_address_id  = lookup(frontend_ip_configuration.value, "public_ip_address_id", null)
    }
  }
}

resource "azurerm_lb_backend_address_pool" "backend_pool" {
  for_each = toset(var.backend_pool_names)
  resource_group_name = var.resource_group_name
  loadbalancer_id     = azurerm_lb.azlb.id
  name                = each.key
}

resource "azurerm_lb_probe" "probes" {
  for_each = var.azlb_probes
  resource_group_name = var.resource_group_name
  loadbalancer_id     = azurerm_lb.azlb.id
  name                = each.key
  port = each.value.port 
  protocol = lookup(each.value, "protocol", null)
  request_path = lookup(each.value, "request_path", null)
  interval_in_seconds = lookup(each.value, "interval_in_seconds", null)
  number_of_probes    = lookup(each.value, "number_of_probes", null)
}

resource "azurerm_lb_rule" "az_lb_rule" {
  for_each = var.azlb_rules
  resource_group_name            = var.resource_group_name
  name                           = each.key 
  loadbalancer_id                = azurerm_lb.azlb.id
  protocol                       = each.value.protocol 
  frontend_port                  = each.value.frontend_port
  backend_port                   = each.value.backend_port
  probe_id                       = azurerm_lb_probe.probes[each.value.probe_name].id
  backend_address_pool_id        = azurerm_lb_backend_address_pool.backend_pool[each.value.backendpool_name].id
  frontend_ip_configuration_name = each.value.frontend_name 
  enable_floating_ip             = lookup(each.value, "enable_floating_ip", false) 
  load_distribution              = lookup(each.value, "load_distribution", null)
  idle_timeout_in_minutes        = lookup(each.value, "idle_timeout_in_minutes", null)
}
